#!/bin/bash

echo inicializar recuperación ...
cd Deploy/backup
sh restore.sh

echo Iniciando Mainflux...
cd ../ui
docker-compose -f docker/docker-compose.yml up -d
echo ejecutando ChirpStack ...
cd ../chirpstack
docker-compose up -d
echo ejecutando LoraDeviceSimuletor ...
cd ../simulador/
./gui &
