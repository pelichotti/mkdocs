# Despligue Ultra Rápido del proyecto y Guia de la documentación usando GitLab Pages


Este proyecto se compone de dos partes, por un lado el repositorio con todos los archivos para realizar un despligue ultra rápido local y por el otro la documentación sobre el despligue del proyecto y por otro lado .

## Despligue Ultra Rápido local del proyecto

Para realizar el despliegue localmente seguir los siguientes pasos:

1. Instale [Docker](https://docs.docker.com/install/) (version 18.09)

2. Instale [Docker compose](https://docs.docker.com/compose/install/) (version 1.24.1)

3. Clonar el repo de este proyecto:
  ```bash
  git clone https://gitlab.com/unrc/aplicacionestcpip/trabajos_finales/pelichotti_nicolas-2020/mkdocs.git
  ```
4. Corra el script run.sh para instalar los volumenes pre-cargods, lanzar las plataformas y el simulador:
  ```bash
  cd mkdocs
  ```
  Luego:
  ```bash
  ./run.sh
  ```

---
### Usuarios y Claves de acceso

- Usuario y clave para acceder a Mainflux son pelichotti@gmail.com / nico2020
- Usuario y clave para acceder a ChirpStack son: admin / admin
- Usuario y clave para acceder a Grafana son: admin / admin

---
## Documentando en GitLab Pages

Para acceder a la documentación del proyecto de IoT hacer click: [Medición de nivel de agua con Tecnología IoT](https://unrc.gitlab.io/aplicacionestcpip/trabajos_finales/pelichotti_nicolas-2020/mkdocs/)


#### Indice de la Documentación del proyecto:


[Introducción](https://unrc.gitlab.io/aplicacionestcpip/trabajos_finales/pelichotti_nicolas-2020/mkdocs/introduccion/)

[1.- Adquisición de Datos](https://unrc.gitlab.io/aplicacionestcpip/trabajos_finales/pelichotti_nicolas-2020/mkdocs/adquisicion_de_datos/)

[2.- Redes de nodos
](https://unrc.gitlab.io/aplicacionestcpip/trabajos_finales/pelichotti_nicolas-2020/mkdocs/redes_de_nodos/)

[3.- Servidor de Red](https://unrc.gitlab.io/aplicacionestcpip/trabajos_finales/pelichotti_nicolas-2020/mkdocs/servidor_de_red/)

[4.- Servidor de Aplicación](https://unrc.gitlab.io/aplicacionestcpip/trabajos_finales/pelichotti_nicolas-2020/mkdocs/servidor_de_aplicacion/)

[5.- Visualización](https://unrc.gitlab.io/aplicacionestcpip/trabajos_finales/pelichotti_nicolas-2020/mkdocs/visualizacion/)

[Práctica](https://unrc.gitlab.io/aplicacionestcpip/trabajos_finales/pelichotti_nicolas-2020/mkdocs/practica/)

[Conclusiones Finales](https://unrc.gitlab.io/aplicacionestcpip/trabajos_finales/pelichotti_nicolas-2020/mkdocs/conclusiones/)

[Anexos](https://unrc.gitlab.io/aplicacionestcpip/trabajos_finales/pelichotti_nicolas-2020/mkdocs/anexos/)

---

Para leer mas sobre GitLab Pages https://pages.gitlab.io y la documentación
ofical está en https://docs.gitlab.com/ce/user/project/pages/.

---
