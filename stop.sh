#!/bin/bash

echo stop Mainflux...
cd Deploy/ui
docker-compose -f docker/docker-compose.yml stop
echo stop ChirpStack ...
cd ../chirpstack
docker-compose stop
