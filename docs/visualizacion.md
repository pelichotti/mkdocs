Por último, daremos una breve descripción de Grafana, que es un software libre open source, que permite la visualización y el formato de datos métricos. Permite crear cuadros de mando y gráficos a partir de múltiples fuentes, incluidas bases de datos de series de tiempo como Graphite, InfluxDB y OpenTSDB. Tiene la posibilidad de crear alertas, alarmas. En nuestro caso leeremos los datos desde la InfluxDB, base de datos que viene por defecto entre los servicios de Mainflux.
En general, la parte del Dashboard queda más explícito a la hora de ponerlo en práctica. Acá una imagen de como quedará nuestra simple visualización.

![](img/grafana.png)
