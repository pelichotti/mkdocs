## 3.- Servidor de Red
La arquitectura de red LoRaWAN se presenta típicamente en una topología de estrella-estrella en la que los gateways son un puente transparente que retransmite mensajes entre nodos (end-devices) y un servidor de red central en el backend (Network Server). Los Gateways están conectados a la red a través de conexiones IP estándar mientras que los dispositivos utilizan la comunicación inalámbrica de un solo salto a uno o varios gateways. Entonces, la responsabilidad del servidor de red es la desduplicación y el manejo de las tramas de enlace ascendente recibidas por los gateways, el tratamiento de la capa de mac LoRaWAN y la programación de transmisiones de datos de enlace descendente.
Hay varios Servidores de Red en el mercado, los más usados y libres son The Things Network y ChirpStack (antes llamado LoraServer).
Usaremos ChirpStack el cual nos proporciona componentes de código abierto para redes LoRaWAN. Incluye una interfaz web fácil de usar para la administración de dispositivos y API para la integración. La arquitectura modular permite integrarse dentro de infraestructuras existentes. Todos los componentes tienen licencia MIT y pueden usarse con fines comerciales. Es una plataforma con básicamente 3 etapas o servicios.

![](img/chirpstack.png)

**ChirpStack Gateway Bridge:** es un servicio que convierte los protocolos de reenvío de paquetes LoRa en un protocolo común de servidor LoRa (JSON).

**Servidor de red ChirpStack:** es el servidor de red LoRaWAN propiamente dicho, y es la parte principal del proyecto LoRa Server. La responsabilidad del componente servidor de red es la desduplicación y el manejo de las tramas de enlace ascendente recibidas recibidas por las puertas de enlace o gateways, el tratamiento de la capa de mac LoRaWAN y la programación de transmisiones de datos de enlace descendente.

**Servidor de aplicaciones ChirpStack:**  Es responsable de la parte del "inventario" de dispositivos de una infraestructura LoRaWAN, del control de las solicitudes de unión y el tratamiento y cifrado de las cargas útiles de las aplicaciones. Ofrece una interfaz web donde se pueden administrar usuarios, organizaciones, aplicaciones y dispositivos. Para la integración con servicios externos, ofrece un API RESTful y también gRPC.

![](img/architecturechirp.png)
