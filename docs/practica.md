En este apartado realizaremos todo el despliegue de los 5 puntos antes mencionados. Simulando 3 sensores lora como si estuviesen conectados a sus respectivos sensores. Veremos paso a paso como en el localhost podemos desplegar los contenedores docker de todos los servicios antes mencionados: ChirpStack y de Mainflux. Además desplegar Grafana y también InfluxDB. Simular 3 nodos, y poder ver dicha información armando un Dashboard de visualización.

## Instalación:

Lo primero es clonar el repositorio en nuestro sistema. Docker y docker-compose son requisitos previos.

~~~
git clone https://github.com/mainflux/mainflux.git
~~~

## Despliegue de Mainflux
Entramos a la carpeta mainflux y corremos:
~~~
docker-compose -f docker/docker-compose.yml up
~~~

Esto abrirá todos los contenedores acoplables Mainflux y los interconectará en la composición. En este ejemplo usaremos InfluxDB como base de datos de mensajes. Por lo tanto, debemos ejecutar la composición de Docker del complemento `influxdb-writer.` Para eso, ejecute estos comandos desde la raíz del proyecto:
~~~
docker-compose -f docker/addons/influxdb-writer/docker-compose up
~~~

## Ejecutar Servidor de Red
Antes de ejecutar lora-adapter debe instalar y ejecutar ChirpStack. Primero, ejecute el siguiente comando:
~~~
git clone git clone https://github.com/brocaar/chirpstack-docker.git
~~~

Una vez que todo esté descargado, ejecute el siguiente comando desde la raíz del proyecto del servidor ChirpStack:
~~~
docker-compose up
~~~

Mainflux y ChirpStack utilizan sus propios brokers MQTT. Por defecto, esos usan el puerto MQTT estándar 1883. Si está ejecutando ambos sistemas en la misma máquina, debe usar puertos diferentes. Puede arreglar esto en el lado de Mainflux configurando la variable de entorno `MF_MQTT_ADAPTER_PORT`.

## Configuración del servidor de Red
Ahora que ambos sistemas se están ejecutando, se debe aprovisionar el servidor ChirpStack, que ofrece integración con servicios externos, una API RESTful y gRPC. La GUI del servidor ofrece una interfaz gráfica para administrar el sistema.
En este punto, suponemos ya que nuestro Gateway está enviando mensajes al servidor LoRa. O sea que nuestro Gateway físico de LoRa está enviando paquetes UDP al IP del LoRa-Gateway-Bridge (donde se ejecuta el servidor LoRa) en el puerto 1700.

## Entonces es momento de:

**Agregar un servidor de redes**
Como cada contenedor tiene su propio nombre de host, debe utilizar el nombre de host del contenedor `networkserver`  al agregar el servidor de red en la interfaz web del servidor de aplicaciones ChirpStack.

Cuando se utiliza el ejemplo anterior, significa que se debe ingresar `chirpstack-network-server:8000` como nombre de host del servidor de red.

[![](img/networkserver.png)](img/networkserver.png)


**Crear una organización:** para agregar sus propias puertas de enlace a la red, debe tener una organización.

[![](img/organization.png)](img/organization.png)

**Crear un perfil de puertas de enlace:** en este perfil puede seleccionar los canales de radio LoRa y el servidor de red LoRa para usar.

[![](img/profilegateway.png)](img/profilegateway.png)

**Crear un perfil de servicio:** un perfil de servicio conecta una organización a un servidor de red y define las características que una organización puede usar en este servidor de red.

[![](img/profileservice.png)](img/profileservice.png)

**Crear una puerta de enlace:** debe establecer la identificación adecuada para que LoRa Server la descubra.

[![](img/gateway.png)](img/gateway.png)
El gateway será creado y accesible desde la página de GATEWAYS:

[![](img/gateway2.png)](img/gateway2.png)

Si se ha encontrado el Gateway, debería ver algo así:

[![](img/gateway3.png)](img/gateway3.png)

**Crear una aplicación de servidor LoRa:** esto le permitirá crear dispositivos conectándolos a esta aplicación. Es el equivalente de Mainflux de conectar dispositivos a canales.

[![](img/application.png)](img/application.png)

**Crear un perfil de dispositivo:** antes de crear un dispositivo, debe crear un perfil de dispositivo donde definirá algún parámetro como la versión MAC de LoRaWAN (formato de la dirección del dispositivo), el parámetro regional de LoRaWAN (banda de frecuencia). Además si el perfil es de dispositivos ABP o OTAA, en nuestro caso serán ABP. Esto le permitirá crear muchos dispositivos usando un mismo perfil.

[![](img/device-profile.png)](img/device-profile.png)

**Crear un dispositivo:** finalmente, puede crear un dispositivo. Edite los nombres de los perfiles, seleccione un perfil de dispositivo y genere su clave de dispositivo:

[![](img/device.png)](img/device.png)

**Active su dispositivo:** Ya que el dispotivo es ABP debe configurar en ACTIVATION `Device address`, el `Network session key` y el `Application session key` de su dispositivo. Puede generarlos y copiarlos en la configuración de su dispositivo o puede usar sus propias claves pregeneradas y configurarlas usando la GUI del servidor de aplicaciones LoRa.

[![](img/device-reactivate.png)](img/device-reactivate.png)

Estos últimos dos pasos se realizaran hasta contar con 3 nodos creados y activados.

[![](img/devices.png)](img/devices.png)

## Ejecuta lora-adapter
Una vez que Mainflux y ChirpStack se estén ejecutando y el ChirpStack esté aprovisionado con los 3 nodos y el gateway, ejecutamos el siguiente comando desde la raíz del proyecto Mainflux para ejecutar el `lora-adapter`:
~~~
docker-compose -f docker/addons/lora-adapter/docker-compose.yml up
~~~

## Aprovisionamiento / Mapa de ruta:
El `lora-adapter `usa la base de datos de Redis para crear un mapa de ruta entre ambos sistemas. Como en Mainflux usamos canales para conectar cosas, ChirpStack usa aplicaciones para conectar dispositivos.
El lora-adapter usa los metadatos de los eventos de provisión emitidos por el sistema Mainflux para actualizar su mapa de ruta. Para eso, se debe proporcionar los Channels and Things con una clave de metadatos adicional en el cuerpo JSON de la solicitud HTTP. Tiene que ser un objeto JSON con las clave `appID` y `devEUI`. En este caso el valor `type` debe ser `lora` y el valor `appID` o `devEUI` debe ser el ID de aplicación LoRa existente o EUI de los dispositivos.

## Disposición a través de GUI:

El aprovisionamiento también se puede realizar desde Mainflux UI, uno de los contenedores de la ventana acoplable Core expuestos en localhost:80. Para crear el mapeo, asegúrese de que los metadatos tengan el formato adecuado, en el siguiente ejemplo `appID` es `33` y `devEUI` es `b2460a47255c771d`.

[![](img/addlorathings.png)](img/addlorathings.png)

Esta acción creará automaticamente un Things y un Channel asociado con su correspondiente ID, en este caso el ID del canal es: `21bd2313-e4ad-475b-b109-dba800965a40 `

[![](img/addchannel.png)](img/addchannel.png)

Aprovisionando los otros 2 nodos, el resultado será algo asi:

[![](img/addalldevices.png)](img/addalldevices.png)

## Mensajería

Para recibir mensajes, el `lora-adapter` se suscribe a un tema específico del broker LoRa Server MQTT que el servicio ChirpStack-Gateways-Bridge usa para publicar paquetes UDP decodificados recibidos desde gateways. Verifica `appID` y `devEUI` de mensajes publicados. Si el mapeo existe, utiliza los correspondientes `channelID` y `thingID` para firmar y reenviar el contenido del mensaje LoRa al agente de mensajes Mainflux.
Siguiendo los pasos anteriores y aprovisionando correctamente la instancia de Mainflux, los mensajes entrantes deberían almacenarse en la base de datos de InfluxDB a través del servicio `mainflux-influxdb-writer`.

## Leer mensajes de LoRa

Para leer los mensajes almacenados, puede usar el servicio `mainflux-grafana` expuesto en `localhost:3001` que se ejecuta automáticamente con la influxdb-writer a través de un docker-compose.

## Configuración de Grafana

Usando su navegador, iniciamos sesión en Grafana con credenciales admin / admin y simplemente agregamos una base de datos de tipo InfluxDB usando la dirección predeterminada http://influxdb:8086. La base de datos, el usuario y la contraseña son mainflux. Estos valores se pueden configurar en el archivo influxdb-writer/docker-compose.yml

[![](img/grafanainfluxdb.png)](img/grafanainfluxdb.png)

Finalmente, se puede agregar diferentes tipos de paneles a un Tablero y configurarlos con diferentes consultas de base de datos para generar gráficos, monitores y muchos otros componentes. Se debe seleccionar la tabla DB messages. Los parámetros de la consulta son channel y publisher, los valores se generaron en el servicio Things dispuesto en la etapa anterior de Mainflux y corresponden a channel ID y deviceID.

[![](img/dashboardgrafana.png)](img/dashboardgrafana.png)

Pudiendo quedar algo similar:

[![](img/dashboardgrafana2.png)](img/dashboardgrafana2.png)

## Simulando devices

Existen muchas herramientas de simulación de dispositivos LoRa, incluso de código abierto, herramientas para pruebas de estrés, para testeos y prubas de robustés de plataformas, borkers, etc. En nuestro caso usaremos una simple. Su nombre es Lora Device Simulator, para su despliegue vamos a su repositorio es: https://github.com/iegomez/lds
~~~
git clone git clone https://github.com/iegomez/lds.git
~~~


Una vez compilado se ejecuta el archivo `gui`. Es bastante intuitivo su funcionamiento.

[![](img/lds.png)](img/lds.png)

En nuestro caso fuimos guardando la configuración para cada nodo. Asi enviamos "data" de cada dispositivo simulando la infomación obtenida de los sensores y nodos. Dos variables solamente tenemos en nuestro ejemplo, batería del dispotivo y nivel medido de agua de la napa freática. 
