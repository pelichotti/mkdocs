Al finalizar el recorrido de todo el proyecto, puedo realizar algunas conclusiones principales:

## Pros

  - Microservicios: Al ser todo realizado con microservicios es interesante el rápido despliegue que tiene todo el proyecto. Pudiendo tener escalabilidad y garantizando alta disponibilidad de los servicios.

  - Independencia: Al ser todo open source y gran parte en software libre y al no depender de plataformas comerciales, el proyecto termina siendo independiente de los mismos, pudiendo desplegar en cualquier proveedor de servidores comerciales en la nube o en Servidores locales o propios. O migrar en función de los requerimientos y el desarrollo del proyecto.

  - Definiciones: Las decisiones sobre qué tipo de tecnología de radio usar, que tipo de servidores de red y que tipo de servidores de aplicación, fueron tomadas en función de realizar un pequeño recorrido exploratorio.

## Contras

  - El servidor de aplicación tiene execelentes respuestas en términos de robustés y seguridad. Pero aún se encuentra en versiones "beta" ya que no existe a la fecha la versión 1, solo van por la versión 0.11.

  - Poca información, como es sabido, el esquema de negocios del servidor de Aplicación es abierto, pero si quiere soporte, es de pago. De esta manera quedan afuera muchas consultas y/o mejoras, que solo se pueden resolver mas lento o a través de la comunidad.
