En esta etapa inicial el desafío está en proponer una manera eficaz de adquirir los datos,  se trata de medir la distancia desde la superficie donde se encuentra el nodo hasta la napa freática. Dos aspectos son centrales, **uno** es tener en cuenta la problemática a asumir, las inclemencias del tiempo y que el sensor debe estar al aire libre en medio del lote de un campo. Y **dos** , los requerimientos necesarios en términos de precisión y eficacia de la medición, pero también requerimientos asociados a la siguientes etapas. Un aspecto a tener en cuenta es la de no tener grandes consumos de energía ya que la provisión de los nodos es de baterias.
En función de todos estos puntos, se determina un grupo de sensores analógicos, de membrana, de forma de sonda, capaces de medir profundidad en tanques de líquidos. Los cuales básicamente miden la presión que ejerce el agua sobre la membrana, esta presión es proporcional a la columna de agua por arriba del sensor y es traducida a niveles de corriente que van de los 4 a los 20 miliamperes.
Según el fabricante, estos son los datos técnicos:

- Circuito completamente cerrado, sin humedad, condensación, función antifugas
- Rango: 0 ～ 2000 metros
- Encapsulado de una pieza
- Precisión: 0.5% FS
- El tamaño es de aproximadamente 145 mm, diámetro 28 mm
- Forma y rango de medición 0 ～ 1 ～ 200mH2O
- Permite sobrecarga 2 veces la presión de escala máxima
- Temperatura de trabajo -20 ～ 60 ° C
- Compensación de la temperatura -10 ～ 60 ° C
- Temperatura de almacenamiento -40 80 ° C
- Señal de salida 4-20mA
- Fuente de alimentación 15 ~ 36 VDC (voltaje estándar 24VDC)
- Material de la carcasa de acero inoxidable 304
- Cable impermeable y transpirable
- Clase de protección IP68

![](img/sensor.png)
