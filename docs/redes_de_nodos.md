## 2.1.- LPWAN
En este punto nos definimos por un sistema de redes LPWAN (Low Power Wide Area Network) ya que tienen 3 características fundamentales que van con nuestros requisitos para éste proyecto:

- El alcance: LPWAN está diseñado para el transporte inalámbrico de datos entre dispositivos separados por distancias en el rango de kilómetros y no de metros.

- La cantidad de data transmitida: La idea de LPWAN es regular el transporte no constante de pequeñas cantidades de datos.

- El bajo consumo eléctrico: El protocolo se fundamenta en el uso de dispositivos cuyas baterías permiten una duración de años en lugar de semanas y meses.

Por otro lado, un punto técnico interesante de mencionar sobre LPWAN es la banda de radiofrecuencia en la cual suelen trabajar.
Los diferentes productos que implementan LPWAN trabajan en Europa en la banda ISM (Industrial, Scientific and Medical), la cual representa un espectro que internacionalmente se reserva al uso no comercial asociado con la industria, la ciencia y los servicios médicos.
Esta banda es importante porque este grupo de frecuencias pueden ser utilizadas sin el pago por concepto de licencia, siempre y cuando se respeten las restricciones en los niveles de potencia transmitidas.
Hay una serie de estándares y proveedores que compiten en el espacio LPWAN, acá van los puntos más destacados:

- LoRa: es una tecnología patentada de modulación de radio de espectro expandido (CSS) para LPWAN utilizada por LoRaWAN, Haystack Technologies y Symphony Link.
- LoRaWan: es un protocolo de capa de control de acceso a medios para gestionar la comunicación entre las pasarelas LPWAN y los dispositivos de nodo final, mantenidos por LoRa Alliance.
- Ultra Narrow Band (UNB): es una tecnología de modulación utilizada por LPWAN por varias compañías entre ellas Sigfox
- Otros: DASH7, MySensors, NarrowBand IoT (NB-IoT), etc…

![](img/lora2.png)

## 2.2.- LoRa
LoRa es el tipo de modulación en radiofrecuencia patentado por Semtech y que entre sus principales ventajas se encuentra:

- Alta tolerancia a las interferencias
- Alta sensibilidad para recibir datos (-168dB)
- Basado en modulación chirp
- Bajo Consumo (hasta 10 años con una batería*)
- Largo alcance 10 a 20km
- Baja transferencia de datos (hasta 255 bytes)
- Conexión punto a punto
- Frecuencias de trabajo: 915Mhz América, 868 Europa, 433 Asia

Todo esto hace a la tecnología ideal para conexiones a grandes distancias y para redes de IoT que se pueden utilizar en ciudades inteligentes, lugares con poca cobertura de telefonía móvil o redes privadas de sensores o actuadores.

## 2.3.- LoRaWAN
A su vez, las señales de radiofrecuencia van montadas en un protocolo de red “LoRaWAN” que usa la tecnología LoRa para comunicar y administrar dispositivos LoRa, se compone de dos partes principalmente: gateways y nodos, los primeros son los encargados de recibir y enviar información a los nodos y los segundos, son los dispositivos finales que envían y reciben información hacia el gateway.

![](img/Technology_stack.png)

### 2.3.1.- Características

Las principales características de LoRaWAN son:
- Topología estrella
- Alcance de 10 a 15 km en línea de vista
- Encriptación AES 128
- Soporte para 3 clases de nodos
- Administración de dispositivos
- Redes públicas y privadas
- Bajo consumo y largo alcance
- Baja transferencia de datos (hasta 242 bytes)

### 2.3.2.- Arquitectura
La arquitectura de red típica, es una red de Redes en Estrella, de forma que la primera estrella está formada por los dispositivos finales y las puertas de enlace, y la segunda estrella está formada por las puertas de enlace y un servidor de red central. En este caso las puertas de enlaces son un puente transparente entre los dispositivos finales y el servidor de red central. Uno o más dispositivos finales se conectan a una o más puertas de enlace, mediante una conexión inalámbrica de un solo salto, usando tecnología RF LoRa o FSK, formando así una red en estrella.

![](img/concentrador.png)

Una o más puertas de enlace se conectan al servidor de red central por medio de conexiones IP estándar, formando así una red en estrella. Las comunicaciones entre los dispositivos y el servidor de red, son generalmente unidireccionales o bidireccionales, pero el estándar también soporta multidifusión, permitiendo la actualización de software en forma inalámbrica, u otras formas de distribución de mensajes en masa.

![](img/redes.png)

En la imagen anterior puedes ver claramente cómo se compone una red LoRaWAN clásica, en la que una serie de dispositivos finales se conectan a Gateways y estos envían todo a un servidor de Red, que por medio de una API entrega los datos a un servidor de Aplicación.

![](img/lorawan.svg)

### 2.3.3.- Clases de nodos
En protocolo LoRaWAN existen tres tipos de clases de nodo:

**Clase A:** La más soportada en casi todos los dispositivos, este tipo de clase ofrece el mayor ahorro de energía debido a que solo entra en modo escucha (llamado ventana RX) después de enviar un datos hacia el gateway, por eso es ideal para dispositivos que usan una batería.

**Clase B:** Este tipo de dispositivos tiene las ventanas de recepción con base a tiempos predeterminados con el gateway, este tipo de nodos puede usar una batería o una fuente externa dependiendo de los tiempos asignados de escucha. Hasta el momento no conozco ningún servicio para lorawan que soporte esta clase hasta el momento

**Clase C:** Este tipo de clase ofrece el menor ahorro de energía debido a que siempre está en modo escucha y solo cuando es necesario en modo transmitir, la recomendación es usarlo en dispositivos que cuentan con una fuente externa de alimentación.

### 2.3.4.- Conexión ABP y OTAA
ver que poner

### 2.3.5.- Características de un Gateway
ver que poner

### 2.3.6.- Dragino
En virtud de lo antes planteado, optamos por adquirir tecnología china de la empresa Dragino. Tanto los gateways como los nodos. Acá dejamos algunas fotos y sus características técnicas.

**Nodos:**

- STM32L072CZT6 MCU
- SX1276/78 LoRa Wireless Modem
- Pre-load with ISP bootloader
- I2C,LPUSART1,USB
- 18 x Digital I/Os
- 2 x 12bit ADC; 1 x 12bit DAC
- MCU wake up by UART or Interrupt
- LoRa™ Modem
- Preamble detection
- Baud rate configurable
- LoRaWAN 1.0.2 Specification
- Software base on STM32Cube HAL drivers
- Open source hardware / software
- Available Band:433/868/915/920 Mhz
- IP66 Waterproof Enclosure
- Ultra Low Power consumption
- AT Commands to setup parameters
- 4000mAh Battery for Long term use

![](img/lsn50.jpg)

**Gateway:**

Linux Side:

  - Processor: 400MHz, 24K MIPS
  - Flash: 16MB ; RAM: 64MB

MCU/LoRa Side:

  - MCU: ATMega328P
  - Flash:32KB, RAM:2KB
  - LoRa Chip: SX2176/78

Interfaces:

  - 10M/100M RJ45 Ports x 2, Support Passive PoE
  - WiFi : 802.11 b/g/n
  - LoRa Wireless
- Power Input: 12V DC
- USB 2.0 host connector x 1
- USB 2.0 host internal interface x 1

![](img/Dragino-OLG01.jpg)
