## 4.1.- Esquema General
Existen muchas plataformas IoT disponibles para nuestros desarrollos, para nuestro caso de estudio se explicará el esquema de la plataforma IoT Mainflux. También de desarrollo open source, escrita en GO.
El diagrama general de conexión desde el sensor hasta el software de aplicación es el siguiente:

![](img/diagrama.png)

## 4.2.- Lora Adapter
Este adaptador se encuentra entre Mainflux y LoRa Server y reenvía los mensajes MQTT publicados por LoRa App Server al intermediario de mensajes multiprotocolo Mainflux, utilizando los tópicos MQTT adecuados y en el formato de mensaje correcto (JSON y SenML), es decir, respetando las API de ambos sistemas.

![](img/loraadpter.png)

## 4.3.- Mainflux
Mainflux es una plataforma IoT de código abierto segura y de alto rendimiento con las capacidades completas a gran escala para el desarrollo de soluciones de Internet de las cosas, aplicaciones IoT y productos inteligentes conectados.
Construido como un conjunto de microservicios en contenedores Docker y orquestado con Kubernetes, la plataforma Mainflux IoT sirve como infraestructura de software y middleware que proporciona:

- Gestión de dispositivos
- Agregación de datos y gestión de datos
- Conectividad y enrutamiento de mensajes
- Gestión de eventos
- Analítica central
- Interfaz de usuario
- Habilitación de aplicaciones

**Algunas características más:**

Código abierto y libre de patentes. Licencia de Apache 2.0. Todos los beneficios de las soluciones de código abierto: transparencia, control, pruebas comunitarias, soporte y corrección de errores. No hay problemas de licencias de acceso de clientes. Sin cerradura de vendedor.
Seguro. Seguridad mejorada y detallada a través del Servidor de autenticación y Autorización Mainflux, listo para la implementación con esquema de Control de Acceso basado en claves API personalizables y de ámbito JWT. Autenticación mutua de TLS (mTLS) utilizando certificados X.509. Proxy inverso NGINX para seguridad, equilibrio de carga y terminación de conexiones TLS y DTLS.
Mainflux es una plataforma de nube de IoT moderna, escalable, segura, de código abierto y sin patentes escrita en el lenguaje de programación Go. Acepta conexiones a través de múltiples protocolos de red (es decir, HTTP, MQTT, WebSocket), creando así un puente continuo entre ellos. Está destinado a ser utilizado como middleware IoT sobre el cual se pueden construir soluciones sofisticadas de IoT.
Junto con la mensajería central, la plataforma Mainflux contiene tres conceptos principales:

### 4.3.1.- Usuarios, Cosas y Canales:
**Usuarios** representa un usuario humano del sistema que se identifica por correo electrónico y contraseña y se utiliza para autenticar al usuario. Para administrar otros recursos (Cosas y Canales), un usuario debe iniciar sesión. La administración de recursos incluye la creación, edición y eliminación de canales y cosas. Así como la conexión de cosas a canales para establecer un conducto de comunicación.

Las **cosas** representan varios dispositivos y aplicaciones que se describen por su ID, propietario (el usuario que los creó), tipo (aplicación o dispositivo), nombre, clave (identificador único que se explicará a continuación).

El **canal** conecta las cosas y, por lo tanto, los canales son bastante simples: tienen una identificación, el propietario (el usuario que los creó), el nombre y la lista de cosas conectadas. Solo las cosas conectadas al mismo canal pueden comunicarse entre sí.

### 4.3.2.- Intercambio de mensajes:
Vamos a explicar aquellos componentes de la plataforma relacionados con el intercambio de mensajes. En la siguiente imagen, puede ver un diagrama de intercambio de mensajes en la plataforma Mainflux.

![](img/nats.png)

Antes de comenzar a describir este diagrama paso a paso, hay algunas cosas relacionadas con la arquitectura que se deben saber. En primer lugar, aunque en el diagrama no se vé, en realidad se compone de dos partes. Hay dos grupos de componentes de Mainflux: componentes de la plataforma central y componentes opcionales. Los componentes principales de la plataforma son Protocol Adapter, NATS y Normalizer. El Writer y la Base de Datos son componentes opcionales. La Thing representa una aplicación o dispositivo que utiliza Mainflux para el intercambio de mensajes y, por lo tanto, no pertenece a ninguno de estos grupos.
Existen muchos estándares y protocolos industriales diferentes para el intercambio de mensajes. Cada uno de esos protocolos especifica diferentes requisitos. La plataforma Mainflux está hecha teniendo eso en mente. De hecho, la plataforma está hecha para admitir varios protocolos utilizando el adaptador de protocolo apropiado. Al momento, los adaptadores de protocolo compatibles que hay son HTTP, WebSocket y MQTT. Para cada uno de los protocolos admitidos, hay un adaptador de protocolo correspondiente a cargo del intercambio de mensajes utilizando ese protocolo específico.
Los mensajes enviados mediante el protocolo HTTP se enviarán al adaptador HTTP, los mensajes MQTT se enviarán al adaptador MQTT, etc. El trabajo del adaptador es transformar un mensaje específico del protocolo en el formato adecuado para su posterior procesamiento. Entonces, el adaptador de protocolo convierte el mensaje específico del protocolo en un mensaje de Mainflux.
NATS es un sistema de mensajería escalable de código abierto que Mainflux usa internamente para el intercambio de mensajes dentro de la plataforma misma.
Una vez publicado en NATS, el mensaje se reenviará al servicio Normalizador. El Normalizer normaliza el mensaje. Como ya dijimos, Mainflux puede recibir cualquier tipo de mensaje y esos mensajes pueden estar en muchos formatos diferentes, que contienen varios campos de datos y metadatos. El servicio normalizador se utiliza para convertir un mensaje al formato SenML. SenML significa Sensor Markup Language y representa el tipo de medio para representar mediciones simples del sensor y parámetros del dispositivo. El servicio normalizador reenviará el mensaje a NATS para su posterior procesamiento, ya sea si es SenML o no. Sin embargo, solo se pueden escribir mensajes válidos de SenML en una base de datos.
El último paso es guardar mensajes en la base de datos. Sin embargo, puede hacer lo que quiera con sus mensajes, por lo que este paso es opcional.
Los servicios de Writer se utilizan para escribir mensajes de Mainflux en la base de datos. Los writers son como adaptadores de protocolo: dado que hay muchos sistemas de bases de datos diferentes, hay un writer correspondiente para cada uno de ellos. Al momento de publicar este artículo, habrá soporte de writers para InfluxDB, Cassandra y MongoDB.
Así quedaría el esquema completo:

![](img/diagramacompleto.jpg)
