Algunos links de información que usé para este trabajo, ademas de repositorios.

- [NB-IoT vs LoRa vs SigFox, Comparando las tecnologías LPWAN más modernas](https://alfaiot.com/blog/ultimas-noticias-2/post/nb-iot-vs-lora-vs-sigfox-10){target="_blank" }

- [LoRaServer (ChirpStack) vs The Things Network (TTN), Redes LoRaWAN privadas y públicas](https://alfaiot.com/blog/ultimas-noticias-2/post/loraserver-chirpstack-vs-the-things-network-ttn-14){target="_blank" }

- [Tutorial de Mainflux LoRaWAN](https://medium.com/mainflux-iot-platform/mainflux-lorawan-tutorial-c54632ffee99){target="_blank" }

- [Configuración y uso de la plataforma Mainflux Open Source IoT](https://medium.com/mainflux-iot-platform/mainflux-open-source-iot-platform-set-up-and-usage-70bed698791a){target="_blank" }

- [Formato SenML](https://tools.ietf.org/html/draft-ietf-core-senml-08){target="_blank" }

- [GitHub de Mainflux](https://github.com/mainflux/mainflux){target="_blank" }

- [Documentación Oficial de Mainflux](https://mainflux.readthedocs.io/en/latest/){target="_blank" }

- [Página Oficial ChirpStack](https://www.chirpstack.io/){target="_blank" }

- [GitHub de ChirpStack](https://github.com/brocaar/chirpstack-docker){target="_blank" }

- [GitHub de Lora Simulator Device](https://github.com/iegomez/lds){target="_blank" }

- [Web Oficial de Dragino](https://www.dragino.com/){target="_blank" }
