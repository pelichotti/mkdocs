# Introducción

El proyecto como ya se planteó en el [Resumen](https://gitlab.com/unrc/aplicacionestcpip/trabajos_finales/pelichotti_nicolas-2020/propuesta_trabajo_final#resumen-del-trabajo), trata de poder desarrollar un sistema de medición de nivel de agua en un campo agrícola, desplegar una red de nodos+sensores que tengan la capacidad de sistematicamente enviar a través de ondas de radio el valor del nivel de altura a la que se encuentra la napa freática. Cada nodo está a kilómetros de distancia entre ellos y entre ellos y el gateway, que es el encargado de recopilar todas las mediciones y encaminarlas hacia la próxima etapa, la de los servidores. En los servidores, se configura también un red (virtual en este caso) donde los datos entran a una plataforma para que luego de la validación se escriban en base de datos y estén listos para su posterior visualización.

Este trabajo consta de la explicación detallada de todo este proceso, como así también de la demostración práctica del despliegue de todo el sistema.

Hemos dividido el sistema en 5 etapas. Donde abordaremos soluciones al objetivo antes planteado, "poder medir las napas freáticas de un campo". En el desarrollo de este trabajo tomaremos decisiones y definiciones, que de alguna medida se hicieron investigando y explorando pros y contras de cada una de las determinaciones adoptadas, además algo de sentido común y también prueba y error. De todas maneras, dejamos algunas conclusiones, datos y características justificando éstas  determinaciones.

En la parte práctica, consta del despligue de todo el sistemas, sus serivicios y ponerlo en marcha, aprovisionar con nodos + gateways simulados y poder tener algunas visualizaciones al final.

Con todo éste recorrido realizado, suponemos una amplia cantidad de información y conocimiento adquirido, demostrando el manejo de éstas tecnologías y esperando estar a la altura de lo que la cátedra pretende.
