#!/bin/bash
# -*- ENCODING: UTF-8 -*-

echo comenzando restauración de volúmenes ...

echo restaurando mainflux-auth-redis-volume
cat docker_mainflux-auth-redis-volume.tar.bz2 | docker run -i -v docker_mainflux-auth-redis-volume:/volume --rm loomchild/volume-backup restore -

echo restaurando mainflux-authn-db-volume
cat docker_mainflux-authn-db-volume.tar.bz2 | docker run -i -v docker_mainflux-authn-db-volume:/volume --rm loomchild/volume-backup restore -

echo restaurando mainflux-bootstrap-db-volume
cat docker_mainflux-bootstrap-db-volume.tar.bz2 | docker run -i -v docker_mainflux-bootstrap-db-volume:/volume --rm loomchild/volume-backup restore -

echo restaurando mainflux-es-redis-volume
cat docker_mainflux-es-redis-volume.tar.bz2 | docker run -i -v docker_mainflux-es-redis-volume:/volume --rm loomchild/volume-backup restore -

echo restaurando mainflux-grafana-volume
cat docker_mainflux-grafana-volume.tar.bz2 | docker run -i -v docker_mainflux-grafana-volume:/volume --rm loomchild/volume-backup restore -

echo restaurando mainflux-influxdb-volume
cat docker_mainflux-influxdb-volume.tar.bz2 | docker run -i -v docker_mainflux-influxdb-volume:/volume --rm loomchild/volume-backup restore -

echo restaurando mainflux-mqtt-broker-volume
cat docker_mainflux-mqtt-broker-volume.tar.bz2 | docker run -i -v docker_mainflux-mqtt-broker-volume:/volume --rm loomchild/volume-backup restore -

echo restaurando mainflux-opcua-redis-volume
cat docker_mainflux-opcua-redis-volume.tar.bz2 | docker run -i -v docker_mainflux-opcua-redis-volume:/volume --rm loomchild/volume-backup restore -

echo restaurando mainflux-things-db-volume
cat docker_mainflux-things-db-volume.tar.bz2 | docker run -i -v docker_mainflux-things-db-volume:/volume --rm loomchild/volume-backup restore -

echo restaurando mainflux-twins-db-volume
cat docker_mainflux-twins-db-volume.tar.bz2 | docker run -i -v docker_mainflux-twins-db-volume:/volume --rm loomchild/volume-backup restore -

echo restaurando mainflux-users-db-volume
cat docker_mainflux-users-db-volume.tar.bz2 | docker run -i -v docker_mainflux-users-db-volume:/volume --rm loomchild/volume-backup restore -

echo restaurando chirpstack_postgresqldata
cat chirpstack_postgresqldata.tar.bz2 | docker run -i -v chirpstack_postgresqldata:/volume --rm loomchild/volume-backup restore -

echo restaurando chirpstack_redisdata
cat chirpstack_redisdata.tar.bz2 | docker run -i -v chirpstack_redisdata:/volume --rm loomchild/volume-backup restore -
